﻿namespace uMod.Common
{
    /// <summary>
    /// Singleton class
    /// ISingleton does not implement singleton behavior
    /// ISingleton informs uMod that there should be only one instance of the implied type in the application scope
    /// This is useful for performance of objects that only have one instance
    /// </summary>
    public interface ISingleton { }
}
