﻿using System;

namespace uMod.Common
{
    /// <summary>
    /// Represents a point in 2D space
    /// </summary>
    public class Point
    {
        public float X, Y;

        /// <summary>
        /// Create empty point object
        /// </summary>
        public Point()
        {
        }

        /// <summary>
        /// Create point object
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public Point(float x, float y)
        {
            X = x; Y = y;
        }

        /// <summary>
        /// Gets distance between two points
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public static float Distance(Point start, Point end)
        {
            return (float)Math.Sqrt(Math.Pow((end.X - start.X), 2) + Math.Pow((end.Y - start.Y), 2));
        }

        /// <summary>
        /// Try to parse a string as point
        /// </summary>
        /// <param name="value"></param>
        /// <param name="point"></param>
        /// <returns></returns>
        public static bool TryParse(string value, out Point point)
        {
            point = null;
            if (string.IsNullOrEmpty(value))
            {
                return false;
            }

            value = value.Replace(",", "");
            value = value.Replace("(", "");
            value = value.Replace(")", "");
            if (value.IndexOf(' ') <= -1)
            {
                return false;
            }

            string[] parts = value.Split(' ');
            if (!float.TryParse(parts[0], out float x))
            {
                return false;
            }

            if (!float.TryParse(parts[1], out float y))
            {
                return false;
            }

            point = new Point(x, y);
            return true;
        }

        /// <summary>
        /// Determine equality of specified object to Point
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if (obj is Point pos)
            {
                return X.Equals(pos.X) && Y.Equals(pos.Y);
            }

            return false;
        }

        /// <summary>
        /// Point equals operator
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool operator ==(Point a, Point b)
        {
            if (ReferenceEquals(a, b))
            {
                return true;
            }

            if ((object)a == null || (object)b == null)
            {
                return false;
            }

            return a.X.Equals(b.X) && a.Y.Equals(b.Y);
        }

        /// <summary>
        /// Point not equals operator
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool operator !=(Point a, Point b)
        {
            return !(a == b);
        }

        /// <summary>
        /// Point addition operator
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static Point operator +(Point a, Point b)
        {
            return new Point(a.X + b.X, a.Y + b.Y);
        }

        /// <summary>
        /// Point subtraction operator
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static Point operator -(Point a, Point b)
        {
            return new Point(a.X - b.X, a.Y - b.Y);
        }

        /// <summary>
        /// Point multiplication operator
        /// </summary>
        /// <param name="mult"></param>
        /// <param name="a"></param>
        /// <returns></returns>
        public static Point operator *(float mult, Point a)
        {
            return new Point(a.X * mult, a.Y * mult);
        }

        /// <summary>
        /// Point multiplication operator
        /// </summary>
        /// <param name="a"></param>
        /// <param name="mult"></param>
        /// <returns></returns>
        public static Point operator *(Point a, float mult)
        {
            return new Point(a.X * mult, a.Y * mult);
        }

        /// <summary>
        /// Point division operator
        /// </summary>
        /// <param name="a"></param>
        /// <param name="div"></param>
        /// <returns></returns>
        public static Point operator /(Point a, float div)
        {
            return new Point(a.X / div, a.Y / div);
        }

        /// <summary>
        /// Get Point HashCode
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode() => new { X, Y }.GetHashCode();

        /// <summary>
        /// Get string representation of Point
        /// </summary>
        /// <returns></returns>
        public override string ToString() => $"({X}, {Y})";

        /// <summary>
        /// Generic empty Point
        /// </summary>
        public static Point Empty { get; } = new Point();
    }
}
