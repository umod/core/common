﻿namespace uMod.Common
{
    /// <summary>
    /// Represents vector position in 4D space
    /// </summary>
    public class Position4 : Position
    {
        public float T;

        /// <summary>
        /// Create empty position4 object
        /// </summary>
        public Position4()
        {
        }

        /// <summary>
        /// Create position4 object
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <param name="t"></param>
        public Position4(float x, float y, float z, float t) : base(x, y, z)
        {
            T = t;
        }

        /// <summary>
        /// Try to parse a string as position(4)
        /// </summary>
        /// <param name="value"></param>
        /// <param name="point"></param>
        /// <returns></returns>
        public static bool TryParse(string value, out Position4 point)
        {
            point = null;
            if (string.IsNullOrEmpty(value))
            {
                return false;
            }

            value = value.Replace(",", "");
            value = value.Replace("(", "");
            value = value.Replace(")", "");
            if (value.IndexOf(' ') <= -1)
            {
                return false;
            }

            string[] parts = value.Split(' ');
            if (parts.Length < 4)
            {
                return false;
            }

            if (!float.TryParse(parts[0], out float x))
            {
                return false;
            }

            if (!float.TryParse(parts[1], out float y))
            {
                return false;
            }

            if (!float.TryParse(parts[2], out float z))
            {
                return false;
            }

            if (!float.TryParse(parts[3], out float t))
            {
                return false;
            }

            point = new Position4(x, y, z, t);
            return true;
        }

        /// <summary>
        /// Determine equality of specified object to Position4
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if (obj is Position4 pos)
            {
                return X.Equals(pos.X) && Y.Equals(pos.Y) && Z.Equals(pos.Z) && T.Equals(pos.T);
            }

            return false;
        }

        /// <summary>
        /// Position4 equals operator
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool operator ==(Position4 a, Position4 b)
        {
            if (ReferenceEquals(a, b))
            {
                return true;
            }

            if ((object)a == null || (object)b == null)
            {
                return false;
            }

            return a.X.Equals(b.X) && a.Y.Equals(b.Y) && a.Z.Equals(b.Z) && a.T.Equals(b.T);
        }

        /// <summary>
        /// Position4 not equals operator
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool operator !=(Position4 a, Position4 b)
        {
            return !(a == b);
        }

        /// <summary>
        /// Position4 addition operator
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static Position4 operator +(Position4 a, Position4 b)
        {
            return new Position4(a.X + b.X, a.Y + b.Y, a.Z + b.Z, a.Z + b.Z);
        }

        /// <summary>
        /// Position4 subtraction operator
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static Position4 operator -(Position4 a, Position4 b)
        {
            return new Position4(a.X - b.X, a.Y - b.Y, a.Z - b.Z, a.T - b.T);
        }

        /// <summary>
        /// Position4 multiplication operator
        /// </summary>
        /// <param name="mult"></param>
        /// <param name="a"></param>
        /// <returns></returns>
        public static Position4 operator *(float mult, Position4 a)
        {
            return new Position4(a.X * mult, a.Y * mult, a.Z * mult, a.T * mult);
        }

        /// <summary>
        /// Position4 multiplication operator
        /// </summary>
        /// <param name="a"></param>
        /// <param name="mult"></param>
        /// <returns></returns>
        public static Position4 operator *(Position4 a, float mult)
        {
            return new Position4(a.X * mult, a.Y * mult, a.Z * mult, a.T * mult);
        }

        /// <summary>
        /// Position4 division operator
        /// </summary>
        /// <param name="a"></param>
        /// <param name="div"></param>
        /// <returns></returns>
        public static Position4 operator /(Position4 a, float div)
        {
            return new Position4(a.X / div, a.Y / div, a.Z / div, a.T / div);
        }

        /// <summary>
        /// Get Position4 HashCode
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode() => new { X, Y, Z, T }.GetHashCode();

        /// <summary>
        /// Get string representation of Position4
        /// </summary>
        /// <returns></returns>
        public override string ToString() => $"({X}, {Y}, {Z}, {T})";

        /// <summary>
        /// Generic empty Position4
        /// </summary>
        public static new Position4 Empty { get; } = new Position4();
    }
}
