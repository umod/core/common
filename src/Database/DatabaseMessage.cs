﻿using System;

namespace uMod.Common.Database
{
    [Serializable]
    public sealed class DatabaseMessage
    {
        public object Data { get; set; }

        public object ExtraData { get; set; }

        public int Id { get; set; }

        public DatabaseMessageType Type { get; set; }

        public override string ToString()
        {
            return $"{Type}: {Id}";
        }
    }
}
