﻿using System;

namespace uMod.Common.Database
{
    [Serializable]
    public enum DatabaseMessageType
    {
        Result = 0,
        Query = 1,
        Error = 2,
        Exit = 3,
        Ready = 4,
        Connect = 5,
        Disconnect = 6,
        Commit = 7,
        Rollback = 8,
        Status = 9,
        ConnectionCount = 10
    }
}
