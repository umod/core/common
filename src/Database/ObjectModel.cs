﻿using System;
using System.Collections.Generic;

namespace uMod.Common.Database
{
    [Serializable]
    public enum RelationType
    {
        Assignable,
        Dictionary,
        List
    }

    [Serializable]
    public class ModelException : Exception
    {
        public ModelException()
        {
        }

        public ModelException(string message) : base(message)
        {
        }

        public ModelException(string message, Exception inner) : base(message, inner)
        {
        }

        protected ModelException(
            System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) : base(info, context)
        { }
    }

    [Serializable]
    public class QueryException : Exception
    {
        public QueryException()
        {
        }

        public QueryException(string message) : base(message)
        {
        }

        public QueryException(string message, Exception inner) : base(message, inner)
        {
        }

        protected QueryException(
            System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) : base(info, context)
        { }
    }

    [Serializable]
    public class ObjectModelRelation
    {
        public RelationType Type;
        public ObjectModel Model;
        public string LocalFieldName;
        public string ForeignFieldName;

        public ObjectModelRelation()
        {

        }

        public ObjectModelRelation(ObjectModel model, string localField = null, string foreignField = null, RelationType type = RelationType.Assignable)
        {
            Model = model;
            Type = type;
            LocalFieldName = localField;
            ForeignFieldName = foreignField;
        }

        public override string ToString()
        {
            return Model?.TypeName ?? string.Empty;
        }
    }

    [Serializable]
    public class ObjectModelListRelation : ObjectModelRelation
    {
        public ObjectModelListRelation()
        {

        }

        public ObjectModelListRelation(ObjectModel model, string localField = null, string foreignField = null) : base(model, localField, foreignField, RelationType.List)
        {

        }
    }

    [Serializable]
    public class ObjectModelDictionaryRelation : ObjectModelRelation
    {
        public ObjectModelDictionaryRelation()
        {

        }

        public ObjectModelDictionaryRelation(ObjectModel model, string localField = null, string foreignField = null) : base(model, localField, foreignField, RelationType.Dictionary)
        {

        }
    }

    [Serializable]
    public class KeyField
    {
        public string TypeName;
        public string Name;

        public KeyField()
        {

        }

        public KeyField(string name, string typeName = null)
        {
            Name = name;
            TypeName = typeName ?? typeof(int).ToString();
        }

        public override string ToString()
        {
            return Name;
        }
    }

    [Serializable]
    public class ObjectModel
    {
        public string TypeName;
        public KeyField PrimaryKeyField;
        public IDictionary<string, ObjectModelRelation> OneToOneRelations;
        public IDictionary<string, ObjectModelRelation> OneToManyRelations;

        public ObjectModel()
        {
        }

        public object GetKey(IDictionary<string, object> @object)
        {
            return @object[PrimaryKeyField.Name];
        }

        public ObjectModelRelation GetRelation(string name)
        {
            if (OneToOneRelations != null && OneToOneRelations.TryGetValue(name, out ObjectModelRelation variable))
            {
                return variable;
            }

            if (OneToManyRelations != null && OneToManyRelations.TryGetValue(name, out variable))
            {
                return variable;
            }

            return null;
        }

        public override string ToString()
        {
            return TypeName;
        }
    }
}
