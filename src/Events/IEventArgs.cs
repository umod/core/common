﻿using System;

namespace uMod.Common
{
    /// <summary>
    /// Event states
    /// </summary>
    public enum EventState
    {
        None = 0,
        Started = 1,
        Canceled = 2,
        Completed = 3,
        Failed = 4,
    }

    public interface IEventArgs : IDisposable
    {
        bool Canceled { get; }
        bool Completed { get; }
        bool Failed { get; }
        bool Started { get; }
        string StateReason { get; }

        IContext GetContext();

        EventState State { get; }
        string StateName { get; }

        void Cancel();

        void Cancel(string reason = "");

        void Fail(Exception ex);

        void Fail();

        void Fail(string message);
    }

    public interface IEventArgs<T> : IEventArgs where T : class, IContext
    {
        T Context { get; }
    }
}
