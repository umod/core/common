﻿namespace uMod.Common
{
    public interface ICallback
    {
        void Call();

        void Remove();
    }

    public interface ICallback<T>
    {
        void Call(T arg0);

        void Remove();
    }

    public interface ICallback<T1, T2>
    {
        void Call(T1 arg0, T2 arg1);

        void Remove();
    }

    public interface ICallback<T1, T2, T3>
    {
        void Call(T1 arg0, T2 arg1, T3 arg2);

        void Remove();
    }

    public interface ICallback<T1, T2, T3, T4>
    {
        void Call(T1 arg0, T2 arg1, T3 arg2, T4 arg3);

        void Remove();
    }

    public interface ICallback<T1, T2, T3, T4, T5>
    {
        void Call(T1 arg0, T2 arg1, T3 arg2, T4 arg3, T5 arg4);

        void Remove();
    }
}
