﻿using System;
using System.Collections.Generic;

namespace uMod.Common
{
    /// <summary>
    /// Represents initialization information relevant to module initialization
    /// </summary>
    public interface IInitializationInfo
    {
        /// <summary>
        /// Gets a list of application names to load
        /// </summary>
        string[] Applications { get; }

        /// <summary>
        /// Gets a dictionary of environment variables
        /// </summary>
        IDictionary<string, string> Environment { get; }

        /// <summary>
        /// Gets a list of extensions to load
        /// </summary>
        Type[] Extensions { get; }

        /// <summary>
        /// Gets the preferred universal provider
        /// </summary>
        Type Provider { get; }
    }
}
