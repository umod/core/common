﻿using System;

namespace uMod.Common
{
    /// <summary>
    /// Represents a game save file
    /// </summary>
    public interface ISaveInfo
    {
        string SaveName { get; }

        DateTime CreationTime { get; }

        uint CreationTimeUnix { get; }

        void Refresh();
    }
}
