﻿using System;
using System.Collections.Generic;
using System.Text;

namespace uMod.Common.Web
{
    /// <summary>
    /// Web request response data
    /// </summary>
    [Serializable]
    public sealed class WebResponse
    {
        public static readonly byte[] EmptyBody = new byte[0];

        /// <summary>
        /// The original method used to get this response
        /// </summary>
        public WebRequestMethod Method;

        /// <summary>
        /// Content-Type set by the Content-Type Header of the response
        /// </summary>
        public string ContentType;

        /// <summary>
        /// The Content-Length returned from the response
        /// </summary>
        public long ContentLength;

        /// <summary>
        /// Gets the Uri of the responding server
        /// </summary>
        public Uri ResponseUri;

        /// <summary>
        /// Gets the status code returned from the responding server
        /// </summary>
        public int StatusCode;

        /// <summary>
        /// Gets information on the returned status code
        /// </summary>
        public string StatusDescription;

        /// <summary>
        /// Gets the HTTP protocol version
        /// </summary>
        public VersionNumber ProtocolVersion;

        /// <summary>
        /// Gets the Content-Encoding from the response
        /// </summary>
        public string ContentEncoding;

        /// <summary>
        /// The Headers from the response
        /// </summary>
        public IDictionary<string, string> Headers;

        /// <summary>
        /// Safely disposes this object
        /// </summary>
        public byte[] Body;

        /// <summary>
        /// Reads the Response in it's raw data
        /// </summary>
        /// <returns></returns>
        public byte[] ReadAsBytes() => ContentLength != 0 ? Body : EmptyBody;

        /// <summary>
        /// Reads the response as a string
        /// </summary>
        /// <param name="encoding"></param>
        /// <returns></returns>
        public string ReadAsString(Encoding encoding = null) => ContentLength != 0 ? encoding?.GetString(ReadAsBytes()) ?? Encoding.UTF8.GetString(ReadAsBytes()) : null;
    }
}
