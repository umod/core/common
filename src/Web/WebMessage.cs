﻿using System;

namespace uMod.Common.Web
{
    /// <summary>
    /// Web message
    /// </summary>
    [Serializable]
    public sealed class WebMessage
    {
        public object Data { get; set; }

        public object ExtraData { get; set; }

        public int Id { get; set; }

        public WebMessageType Type { get; set; }
    }
}
