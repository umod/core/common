using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Threading;
using uMod.Common.IO;
using uMod.Common.Pooling;
using uMod.Common.Threading;

namespace uMod.Common
{
    public sealed class ObjectStreamConnection<TRead, TWrite>
        where TRead : class
        where TWrite : class
    {
        private readonly ObjectStreamWrapper<TRead, TWrite> _streamWrapper;
        private readonly Queue<TWrite> _writeQueue = new Queue<TWrite>();
        private readonly AutoResetEvent _writeSignal = new AutoResetEvent(false);
        private readonly ILogger _logger;
        private readonly IDynamicPool _pool;

        public ObjectStreamConnection(Stream inStream, Stream outStream, SerializationBinder binder = null, ILogger logger = null, IDynamicPool writeObjectPool = null)
        {
            _logger = logger;
            _pool = writeObjectPool;
            _streamWrapper = new ObjectStreamWrapper<TRead, TWrite>(inStream, outStream, binder, logger);
        }

        public event ConnectionMessageEventHandler<TRead, TWrite> ReceiveMessage;

        public event ConnectionExceptionEventHandler<TRead, TWrite> Error;

        public event ConnectionOpenedEventHandler<TRead, TWrite> Connected;

        public event ConnectionClosedEventHandler<TRead, TWrite> Closed;

        public void Open()
        {
            Worker readWorker = new Worker();
            readWorker.Error += OnError;
            readWorker.DoWork(ReadStream);

            Worker writeWorker = new Worker();
            writeWorker.Error += OnError;
            writeWorker.DoWork(WriteStream);

            Connected?.Invoke(this);
        }

        public void PushMessage(TWrite message)
        {
            _writeQueue.Enqueue(message);
            _writeSignal.Set();
        }

        public void Close()
        {
            CloseImpl();
        }

        private void CloseImpl()
        {
            Error = null;
            _streamWrapper.Close();
            _writeSignal.Set();
            Closed?.Invoke(this);
        }

        private void OnError(Exception exception)
        {
            _logger?.Report(exception);
            Error?.Invoke(this, exception);
        }

        private void ReadStream()
        {
            while (_streamWrapper.CanRead)
            {
                TRead obj;
                try
                {
                    obj = _streamWrapper.ReadObject();
                }
                catch (Exception exception)
                {
                    OnError(exception);
                    continue;
                }

                ReceiveMessage?.Invoke(this, obj);

                if (obj != null)
                {
                    continue;
                }

                CloseImpl();
                return;
            }
        }

        private void WriteStream()
        {
            while (_streamWrapper.CanWrite)
            {
                _writeSignal.WaitOne();
                while (_writeQueue.Count > 0)
                {
                    try
                    {
                        TWrite message = _writeQueue.Dequeue();
                        try
                        {
                            _streamWrapper.WriteObject(message);
                        }
                        finally
                        {
                            _pool?.Free(message);
                        }
                    }
                    catch(NotSupportedException nsException)
                    {

                    }
                    catch (Exception exception)
                    {
                        OnError(exception);
                    }
                }
            }
        }
    }

    public static class ConnectionFactory
    {
        public static ObjectStreamConnection<TRead, TWrite> CreateConnection<TRead, TWrite>(Stream inStream, Stream outStream, SerializationBinder binder = null, ILogger logger = null, IDynamicPool writeObjectPool = null)
            where TRead : class
            where TWrite : class
        {
            return new ObjectStreamConnection<TRead, TWrite>(inStream, outStream, binder, logger, writeObjectPool);
        }
    }

    public delegate void ConnectionMessageEventHandler<TRead, TWrite>(ObjectStreamConnection<TRead, TWrite> connection, TRead message)
        where TRead : class
        where TWrite : class;

    public delegate void ConnectionExceptionEventHandler<TRead, TWrite>(ObjectStreamConnection<TRead, TWrite> connection, Exception exception)
        where TRead : class
        where TWrite : class;

    public delegate void ConnectionOpenedEventHandler<TRead, TWrite>(ObjectStreamConnection<TRead, TWrite> connection) where TRead : class where TWrite : class;

    public delegate void ConnectionClosedEventHandler<TRead, TWrite>(ObjectStreamConnection<TRead, TWrite> connection) where TRead : class where TWrite : class;
}
