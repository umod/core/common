﻿using System;
using System.IO;
using System.Runtime.Serialization;
using uMod.Common.IO;
using uMod.Common.Pooling;
using uMod.Common.Threading;

namespace uMod.Common
{
    public class ObjectStreamClient<TReadWrite> : ObjectStreamClient<TReadWrite, TReadWrite> where TReadWrite : class
    {
        public ObjectStreamClient(Stream inStream, Stream outStream, SerializationBinder binder = null, ILogger logger = null, IDynamicPool writeObjectPool = null) : base(inStream, outStream, binder, logger, writeObjectPool)
        {
        }
    }

    public delegate void StreamExceptionEventHandler(Exception exception);

    public class ObjectStreamClient<TRead, TWrite>
        where TRead : class
        where TWrite : class
    {
        private readonly Stream _inStream;
        private readonly Stream _outStream;
        private readonly SerializationBinder _serializationBinder;
        private readonly ILogger _logger;
        private readonly IDynamicPool _pool;
        public ObjectStreamConnection<TRead, TWrite> Connection { get; private set; }
        public event ConnectionMessageEventHandler<TRead, TWrite> Message;
        public event StreamExceptionEventHandler Error;
        public event ConnectionOpenedEventHandler<TRead, TWrite> Connected;
        public event ConnectionClosedEventHandler<TRead, TWrite> Closed;

        public ObjectStreamClient(Stream inStream, Stream outStream, SerializationBinder binder = null, ILogger logger = null, IDynamicPool writeObjectPool = null)
        {
            _inStream = inStream;
            _outStream = outStream;
            _serializationBinder = binder;
            _logger = logger;
            _pool = writeObjectPool;
        }

        public void Start()
        {
            Worker worker = new Worker();
            worker.Error += OnError;
            worker.DoWork(ListenSync);
        }

        public void PushMessage(TWrite message)
        {
            Connection?.PushMessage(message);
        }

        public void Stop()
        {
            Connection?.Close();
        }

        #region Private methods

        private void ListenSync()
        {
            // Create a Connection object for the data pipe
            Connection = ConnectionFactory.CreateConnection<TRead, TWrite>(_inStream, _outStream, _serializationBinder, _logger, _pool);
            Connection.ReceiveMessage += OnReceiveMessage;
            Connection.Error += ConnectionOnError;
            Connection.Connected += Connection_Connected;
            Connection.Closed += Connection_Closed;
            Connection.Open();
        }

        private void Connection_Closed(ObjectStreamConnection<TRead, TWrite> connection)
        {
            Closed?.Invoke(connection);
        }

        private void Connection_Connected(ObjectStreamConnection<TRead, TWrite> connection)
        {
            Connected?.Invoke(connection);
        }

        private void OnReceiveMessage(ObjectStreamConnection<TRead, TWrite> connection, TRead message)
        {
            Message?.Invoke(connection, message);
        }

        private void ConnectionOnError(ObjectStreamConnection<TRead, TWrite> connection, Exception exception)
        {
            OnError(exception);
        }

        private void OnError(Exception exception)
        {
            _logger?.Report(exception);
            Error?.Invoke(exception);
        }

        #endregion Private methods
    }

    public static class ObjectStreamClientFactory
    {
        public static ObjectStreamWrapper<TRead, TWrite> Connect<TRead, TWrite>(Stream inStream, Stream outStream, SerializationBinder binder = null, ILogger logger = null)
            where TRead : class
            where TWrite : class
        {
            return new ObjectStreamWrapper<TRead, TWrite>(inStream, outStream, binder, logger);
        }
    }
}
