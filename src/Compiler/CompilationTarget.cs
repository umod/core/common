﻿using System;

namespace uMod.Common.Compiler
{
    [Serializable]
    public enum CompilationTarget
    {
        ConsoleApplication = 0,
        WindowsApplication = 1,
        DynamicallyLinkedLibrary = 2,
        NetModule = 3,
        WindowsRuntimeMetadata = 4,
        WindowsRuntimeApplication = 5
    }
}
