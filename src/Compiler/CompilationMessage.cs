﻿using System;

namespace uMod.Common.Compiler
{
    [Serializable]
    public sealed class CompilationMessage
    {
        public object Data { get; set; }

        public object ExtraData { get; set; }

        public object ExtraData2 { get; set; }

        public int Id { get; set; }

        public CompilationMessageType Type { get; set; }
    }
}
