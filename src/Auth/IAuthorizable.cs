﻿namespace uMod.Common
{
    /// <summary>
    /// Authorizable interface
    /// </summary>
    public interface IAuthorizable
    {
        /// <summary>
        /// Gets if the player is a server administrator
        /// </summary>
        bool IsAdmin { get; }

        /// <summary>
        /// Gets if the player is a server moderator
        /// </summary>
        bool IsModerator { get; }

        /// <summary>
        /// Determines if the player has the specified permission
        /// </summary>
        /// <param name="permission"></param>
        /// <returns></returns>
        bool Can(string permission);

        /// <summary>
        /// Determines if the player has the specified permission
        /// </summary>
        /// <param name="permission"></param>
        /// <returns></returns>
        bool HasPermission(string permission);

        /// <summary>
        /// Grants the specified permission on this player
        /// </summary>
        /// <param name="permission"></param>
        bool GrantPermission(string permission);

        /// <summary>
        /// Revokes the specified permission from this player
        /// </summary>
        /// <param name="permission"></param>
        bool RevokePermission(string permission);

        /// <summary>
        /// Determines if the player belongs to the specified group
        /// </summary>
        /// <param name="group"></param>
        /// <returns></returns>
        bool BelongsToGroup(string group);

        /// <summary>
        /// Adds the player to the specified group
        /// </summary>
        /// <param name="group"></param>
        bool AddToGroup(string group);

        /// <summary>
        /// Removes the player from the specified group
        /// </summary>
        /// <param name="group"></param>
        bool RemoveFromGroup(string group);
    }
}
