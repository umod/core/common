﻿namespace uMod.Common
{
    public interface IIdentity
    {
        /// <summary>
        /// Gets the unique identification string for the identity
        /// </summary>
        string Id { get; }
    }
}
