﻿using System;
using System.Collections.Generic;

namespace uMod.Common
{
    public interface IPolicyValidator
    {
        IEnumerable<IPolicy> Gates { get; }

        bool Authorize(IGate gate, IContext context, params object[] arguments);
    }

    /// <summary>
    /// Gate Interface
    /// Specifies minimum gate implementation
    /// </summary>
    public interface IGate : IEnumerable<string>
    {
        IEnumerable<string> Policies { get; }

        bool Has(string permission);

        IGate Register(string permission, Func<bool> callback = null);

        bool Allows(string permission);

        bool Allows(string permission, object[] arguments);

        bool Allows<TArg1>(string permission, TArg1 arg1);

        bool Allows<TArg1, TArg2>(string permission, TArg1 arg1, TArg2 arg2);

        bool Allows<TArg1, TArg2, TArg3>(string permission, TArg1 arg1, TArg2 arg2, TArg3 arg3);

        bool Allows<TArg1, TArg2, TArg3, TArg4>(string permission, TArg1 arg1, TArg2 arg2, TArg3 arg3, TArg4 arg4);

        bool Allows<TArg1, TArg2, TArg3, TArg4, TArg5>(string permission, TArg1 arg1, TArg2 arg2, TArg3 arg3, TArg4 arg4, TArg5 arg5);

        bool Denies(string permission);

        bool Denies(string permission, object[] arguments);

        bool Denies<TArg1>(string permission, TArg1 arg1);

        bool Denies<TArg1, TArg2>(string permission, TArg1 arg1, TArg2 arg2);

        bool Denies<TArg1, TArg2, TArg3>(string permission, TArg1 arg1, TArg2 arg2, TArg3 arg3);

        bool Denies<TArg1, TArg2, TArg3, TArg4>(string permission, TArg1 arg1, TArg2 arg2, TArg3 arg3, TArg4 arg4);

        bool Denies<TArg1, TArg2, TArg3, TArg4, TArg5>(string permission, TArg1 arg1, TArg2 arg2, TArg3 arg3, TArg4 arg4, TArg5 arg5);

        bool All(IEnumerable<string> permissions);

        bool All(IEnumerable<string> permissions, object[] arguments);

        bool All<TArg1>(IEnumerable<string> permissions, TArg1 arg1);

        bool All<TArg1, TArg2>(IEnumerable<string> permissions, TArg1 arg1, TArg2 arg2);

        bool All<TArg1, TArg2, TArg3>(IEnumerable<string> permissions, TArg1 arg1, TArg2 arg2, TArg3 arg3);

        bool All<TArg1, TArg2, TArg3, TArg4>(IEnumerable<string> permissions, TArg1 arg1, TArg2 arg2, TArg3 arg3, TArg4 arg4);

        bool All<TArg1, TArg2, TArg3, TArg4, TArg5>(IEnumerable<string> permissions, TArg1 arg1, TArg2 arg2, TArg3 arg3, TArg4 arg4, TArg5 arg5);

        bool Any(IEnumerable<string> permissions);

        bool Any(IEnumerable<string> permissions, object[] arguments);

        bool Any<TArg1>(IEnumerable<string> permissions, TArg1 arg1);

        bool Any<TArg1, TArg2>(IEnumerable<string> permissions, TArg1 arg1, TArg2 arg2);

        bool Any<TArg1, TArg2, TArg3>(IEnumerable<string> permissions, TArg1 arg1, TArg2 arg2, TArg3 arg3);

        bool Any<TArg1, TArg2, TArg3, TArg4>(IEnumerable<string> permissions, TArg1 arg1, TArg2 arg2, TArg3 arg3, TArg4 arg4);

        bool Any<TArg1, TArg2, TArg3, TArg4, TArg5>(IEnumerable<string> permissions, TArg1 arg1, TArg2 arg2, TArg3 arg3, TArg4 arg4, TArg5 arg5);

        bool None(IEnumerable<string> permissions);

        bool None(IEnumerable<string> permissions, object[] arguments);

        bool None<TArg1>(IEnumerable<string> permissions, TArg1 arg1);

        bool None<TArg1, TArg2>(IEnumerable<string> permissions, TArg1 arg1, TArg2 arg2);

        bool None<TArg1, TArg2, TArg3>(IEnumerable<string> permissions, TArg1 arg1, TArg2 arg2, TArg3 arg3);

        bool None<TArg1, TArg2, TArg3, TArg4>(IEnumerable<string> permissions, TArg1 arg1, TArg2 arg2, TArg3 arg3, TArg4 arg4);

        bool None<TArg1, TArg2, TArg3, TArg4, TArg5>(IEnumerable<string> permissions, TArg1 arg1, TArg2 arg2, TArg3 arg3, TArg4 arg4, TArg5 arg5);
    }

    public interface IPermissionGate : IGate
    {
        IPlugin Plugin { get; }

        IEnumerable<string> Permissions { get; }

        bool Allows(string permission, IPlayer player);

        bool Denies(string permission, IPlayer player);
    }

    public interface IPolicy
    {
        Type GateType { get; }
    }

    public interface ISinglePolicy : IPolicy
    {
        string PolicyName { get; }
    }

    public interface IAggregatePolicy : IPolicy
    {
        IEnumerable<string> PolicyNames { get; }
    }
}
