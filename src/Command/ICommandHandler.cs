﻿using System;

namespace uMod.Common.Command
{
    public interface ICommandHandler
    {
        CommandCallback Callback { get; set; }
        Func<string, bool> CommandFilter { get; set; }

        CommandState HandleChatMessage(IPlayer player, string command, object[] context = null);

        CommandState HandleConsoleMessage(IPlayer player, string command, object[] context = null);
    }
}
