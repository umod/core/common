﻿namespace uMod.Common.Command
{
    /// <summary>
    /// Represents a command argument
    /// </summary>
    public struct CommandArgument
    {
        /// <summary>
        /// Represents an empty command argument
        /// </summary>
        public static CommandArgument Empty = new CommandArgument();

        /// <summary>
        /// Gets the argument key
        /// </summary>
        public string Key { get; }

        /// <summary>
        /// Gets the argument value
        /// </summary>
        public object Value { get; }

        /// <summary>
        /// Create a new named command argument
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public CommandArgument(string key = null, object value = null)
        {
            Key = key;
            Value = value;
        }

        /// <summary>
        /// Convert command argument to string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return $"{Key}={Value}";
        }

        /// <summary>
        /// Convert command argument to string implicitly
        /// </summary>
        /// <param name="v"></param>
        public static implicit operator string(CommandArgument v)
        {
            return $"{v.Key}={v.Value}";
        }
    }
}
