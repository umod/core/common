﻿using System.Collections.Generic;
using uMod.Common.Command;

namespace uMod.Common
{
    public interface ICommandInfo : IPolicyValidator
    {
        ICommandDefinition Definition { get; }
        IEnumerable<IPolicy> Gates { get; }
        CommandCallback Callback { get; }
        IDictionary<string, string> Help { get; }
        IDictionary<string, string> Descriptions { get; }

        string GetHelp(string lang = null);

        string GetDescription(string lang = null);
    }

    /// <summary>
    /// Represents a source of commands
    /// </summary>
    public enum CommandType { Chat, Console }

    /// <summary>
    /// Represents the command return state
    /// </summary>
    public enum CommandState { Unrecognized, Canceled, Completed }

    /// <summary>
    /// Represents the callback of a chat or console command
    /// </summary>
    /// <param name="caller"></param>
    /// <param name="cmd"></param>
    /// <param name="fullCommand"></param>
    /// <param name="context"></param>
    /// <param name="cmdInfo"></param>
    /// <returns></returns>
    public delegate CommandState CommandCallback(IPlayer caller, string cmd, string fullCommand, object[] context = null, ICommandInfo cmdInfo = null);

    /// <summary>
    /// Represents a binding to a generic command system
    /// </summary>
    public interface ICommandSystem
    {
        /// <summary>
        /// Registers the specified command
        /// </summary>
        /// <param name="command"></param>
        /// <param name="plugin"></param>
        /// <param name="callback"></param>
        void RegisterCommand(string command, IPlugin plugin, CommandCallback callback);

        /// <summary>
        /// Unregisters the specified command
        /// </summary>
        /// <param name="command"></param>
        /// <param name="plugin"></param>
        void UnregisterCommand(string command, IPlugin plugin);

        /// <summary>
        /// Handles a chat message
        /// </summary>
        /// <param name="player"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        CommandState HandleChatMessage(IPlayer player, string message);
    }
}
