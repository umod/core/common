﻿using System.Collections.Generic;

namespace uMod.Common.Command
{
    /// <summary>
    /// Represents a command definition
    /// </summary>
    public interface ICommandDefinition : INameable
    {
        /// <summary>
        /// Command names
        /// </summary>
        string[] Names { get; }

        /// <summary>
        /// Command aliases
        /// </summary>
        IEnumerable<string> Aliases { get; }

        /// <summary>
        /// Command input arguments
        /// </summary>
        InputArgument[] Arguments { get; }
    }
}
