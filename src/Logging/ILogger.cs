﻿using System;

namespace uMod.Common
{
    /// <summary>
    /// Levels for logger
    /// </summary>
    public enum LogLevel
    {
        Emergency = 0,
        Alert = 1,
        Critical = 2,
        Error = 3,
        Warning = 4,
        Notice = 5,
        Info = 6,
        Debug = 7
    }

    public interface IPluginLogger : ILogger
    {
        IPlugin Plugin { get; }
    }

    public interface IFileLogger : ITextLogger
    {
        string Directory { get; }
        string Path { get; }
    }

    public interface ITextLogger : ILogger
    {
        string Format { get; }
    }

    public interface ILogger
    {
        LogLevel LogLevel { get; }
        IEvent<ILogger> OnConfigure { get; }
        IEvent<ILogger> OnAdded { get; }
        IEvent<ILogger> OnRemoved { get; }

        void Write(LogLevel level, string message);

        void Write(LogMessage message, string defaultMessage = null);

        void Debug(string message);

        void Info(string message);

        void Notice(string message);

        void Warning(string message);

        void Error(string message);

        void Critical(string message);

        void Alert(string message);

        void Emergency(string message);

        void Report(string message, Exception exception);

        void Report(Exception exception);
    }
}
