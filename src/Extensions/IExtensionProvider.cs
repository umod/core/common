﻿using System;
using System.Collections.Generic;

namespace uMod.Common
{
    public interface IExtensionProvider
    {
        IExtensionManager Manager { get; }

        IEnumerable<IExtension> All { get; }

        T Get<T>(string name = null) where T : class, IExtension;

        IExtension Get(string name);

        IExtension this[string name] { get; }

        void RegisterLoader(IPluginLoader loader);

        void UnregisterLoader(IPluginLoader loader);

        IEnumerable<IPluginLoader> GetLoaders();

        bool Load(IExtension extension);

        bool Load(Type extensionType);

        bool Load<T>() where T : IExtension;

        bool Load(string name);

        bool Unload(IExtension extension);

        bool Unload(string name);

        bool Reload(IExtension extension);

        bool Reload(string name);
    }
}
