namespace System.Collections
{
#if NET35

    public interface IStructuralEquatable
    {
        Boolean Equals(Object other, IEqualityComparer comparer);

        int GetHashCode(IEqualityComparer comparer);
    }

#endif
}
