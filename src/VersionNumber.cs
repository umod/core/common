﻿using System;

namespace uMod.Common
{
    /// <summary>
    /// Represents a version in major.minor.patch form
    /// </summary>
    [Serializable]
    public struct VersionNumber : IComparable<VersionNumber>
    {
        public static VersionNumber Empty = new VersionNumber(0, 0, 0);

        // The major, minor and patch version numbers
        public int Major, Minor, Patch;

        /// <summary>
        /// Initializes a new instance of the VersionNumber struct with the specified Version
        /// </summary>
        /// <param name="version"></param>
        public VersionNumber(Version version) : this(version.Major, version.Minor, version.Build)
        {
        }

        /// <summary>
        /// Initializes a new instance of the VersionNumber struct with the specified values
        /// </summary>
        /// <param name="major"></param>
        /// <param name="minor"></param>
        /// <param name="patch"></param>
        public VersionNumber(int major, int minor, int patch)
        {
            Major = major;
            Minor = minor;
            Patch = patch;
        }

        /// <summary>
        /// Returns a human readable string representation of this version
        /// </summary>
        /// <returns></returns>
        public override string ToString() => $"{Major}.{Minor}.{Patch}";

        #region Operator Overloads

        /// <summary>
        /// Compares this version for equality to another
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool operator ==(VersionNumber a, VersionNumber b)
        {
            return a.Major == b.Major && a.Minor == b.Minor && a.Patch == b.Patch;
        }

        /// <summary>
        /// Casts VersionNumber to Version
        /// </summary>
        /// <param name="versionNumber"></param>
        public static implicit operator Version(VersionNumber versionNumber) => new Version(versionNumber.ToString());

        /// <summary>
        /// Casts Version to VersionNumber
        /// </summary>
        /// <param name="version"></param>
        public static explicit operator VersionNumber(Version version) => new VersionNumber(version);

        /// <summary>
        /// Casts string to VersionNumber
        /// </summary>
        /// <param name="input"></param>
        public static implicit operator VersionNumber(string input)
        {
            return TryParse(input, out VersionNumber version) ? version : Empty;
        }

        /// <summary>
        /// Compares this version for inequality to another
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool operator !=(VersionNumber a, VersionNumber b)
        {
            return a.Major != b.Major || a.Minor != b.Minor || a.Patch != b.Patch;
        }

        public static bool operator >(VersionNumber a, VersionNumber b)
        {
            if (a.Major < b.Major)
            {
                return false;
            }

            if (a.Major > b.Major)
            {
                return true;
            }

            if (a.Minor < b.Minor)
            {
                return false;
            }

            if (a.Minor > b.Minor)
            {
                return true;
            }

            return a.Patch > b.Patch;
        }

        public static bool operator >=(VersionNumber a, VersionNumber b)
        {
            if (a.Major < b.Major)
            {
                return false;
            }

            if (a.Major > b.Major)
            {
                return true;
            }

            if (a.Minor < b.Minor)
            {
                return false;
            }

            if (a.Minor > b.Minor)
            {
                return true;
            }

            return a.Patch >= b.Patch;
        }

        public static bool operator <(VersionNumber a, VersionNumber b)
        {
            if (a.Major > b.Major)
            {
                return false;
            }

            if (a.Major < b.Major)
            {
                return true;
            }

            if (a.Minor > b.Minor)
            {
                return false;
            }

            if (a.Minor < b.Minor)
            {
                return true;
            }

            return a.Patch < b.Patch;
        }

        public static bool operator <=(VersionNumber a, VersionNumber b)
        {
            if (a.Major > b.Major)
            {
                return false;
            }

            if (a.Major < b.Major)
            {
                return true;
            }

            if (a.Minor > b.Minor)
            {
                return false;
            }

            if (a.Minor < b.Minor)
            {
                return true;
            }

            return a.Patch <= b.Patch;
        }

        #endregion Operator Overloads

        /// <summary>
        /// Try to parse input string as a version number
        /// </summary>
        /// <param name="version"></param>
        /// <param name="versionNumber"></param>
        /// <returns></returns>
        public static bool TryParse(string version, out VersionNumber versionNumber)
        {
            if (version.IndexOf('.') == -1)
            {
                if (int.TryParse(version, out int major))
                {
                    versionNumber = new VersionNumber(major, 0, 0);
                    return true;
                }

                versionNumber = Empty;
                return false;
            }

            string[] partsString = version.Split('.');
            int[] parts =
            {
                0, 0, 0
            };

            for (int i = 0; i < partsString.Length; i++)
            {
                if (int.TryParse(partsString[i], out int part))
                {
                    parts[i] = part;
                }
                else
                {
                    versionNumber = Empty;
                    return false;
                }
            }

            versionNumber = new VersionNumber(parts[0], parts[1], parts[2]);
            return true;
        }

        /// <summary>
        /// Compares this version for equality to the specified object
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if (obj is VersionNumber other)
            {
                return this == other;
            }

            return false;
        }

        /// <summary>
        /// Gets a hash code for this version
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 17;
                hash = hash * 23 + Major.GetHashCode();
                hash = hash * 23 + Minor.GetHashCode();
                hash = hash * 23 + Patch.GetHashCode();
                return hash;
            }
        }

        /// <summary>
        /// Compare VersionNumber instance with specified VersionNumber
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public int CompareTo(VersionNumber other)
        {
            if (this > other) return -1;
            return this == other ? 0 : 1;
        }
    }
}
