﻿using System;

namespace uMod.Common
{
    /// <summary>
    /// Represents a container for important game-specific type references
    /// </summary>
    public interface IGameTypes
    {
        /// <summary>
        /// Gets the game-specific type associated with a game player (e.g. BasePlayer)
        /// </summary>
        Type Player { get; }

        /// <summary>
        /// Gets an IPlayer from the game-specific player object
        /// </summary>
        /// <param name="gamePlayer"></param>
        /// <returns></returns>
        IPlayer GetPlayer(object gamePlayer);
    }
}
