﻿using uMod.Common.Command;

namespace uMod.Common
{
    /// <summary>
    /// Represents a provider for game-specific universal functionality
    /// </summary>
    public interface IUniversalProvider
    {
        /// <summary>
        /// Gets the name of the game
        /// </summary>
        string GameName { get; }

        /// <summary>
        /// Gets the Steam app ID of the game's client
        /// </summary>
        uint ClientAppId { get; }

        /// <summary>
        /// Gets the Steam app ID of the game's server
        /// </summary>
        uint ServerAppId { get; }

        /// <summary>
        /// Gets the important types in the game's API
        /// </summary>
        IGameTypes Types { get; }

        /// <summary>
        /// Creates the game-specific command system provider object
        /// </summary>
        /// <returns></returns>
        ICommandSystem CreateCommandSystemProvider(ICommandHandler commandHandler);

        /// <summary>
        /// Creates the game-specific player manager object (if one exists)
        /// </summary>
        /// <returns></returns>
        IPlayerManager CreatePlayerManager(IApplication application, ILogger logger);

        /// <summary>
        /// Creates the game-specific server object
        /// </summary>
        /// <returns></returns>
        IServer CreateServer();

        /// <summary>
        /// Formats the text with markup into the game-specific markup language
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        string FormatText(string text);
    }
}
