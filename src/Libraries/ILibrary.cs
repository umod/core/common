﻿using System.Collections.Generic;
using System.Reflection;

namespace uMod.Common
{
    public interface ILibrary : ISingleton
    {
        void Initialize();

        void Shutdown();

        IEnumerable<string> GetFunctionNames();

        IEnumerable<string> GetPropertyNames();

        MethodInfo GetFunction(string name);

        PropertyInfo GetProperty(string name);

        object Get(string name);

        T Get<T>(string name);

        T Set<T>(string name, T @value);

        object Invoke(string name, params object[] parameters);

        T Invoke<T>(string name, params object[] parameters);
    }
}
