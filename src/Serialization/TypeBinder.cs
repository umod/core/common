﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;

namespace uMod.Common.Serialization
{
    /// <summary>
    /// SerializationBinder for faster deserialization by
    /// References to "core" assembly types (those loaded at start) are cached
    /// Type references to "stack" assemblies (those loaded afterward like plugin, extensions) are not cached
    /// Improves the performance of both core type and stack type deserialization
    /// </summary>
    public class TypeBinder : SerializationBinder
    {
        /// <summary>
        /// List of "core" assembly names
        /// </summary>
        private static readonly List<string> ApplicationAssemblies = new List<string>();

        /// <summary>
        /// List of types provided by core assemblies
        /// </summary>
        private readonly Dictionary<string, Type> _cachedTypes = new Dictionary<string, Type>();

        /// <summary>
        /// Create a new type binder
        /// </summary>
        public TypeBinder()
        {
            ApplicationAssemblies.AddRange(AppDomain.CurrentDomain.GetAssemblies().Select(x => x.FullName));
        }

        /// <summary>
        /// Gets type associated with assembly and type
        /// </summary>
        /// <param name="assemblyName"></param>
        /// <param name="typeName"></param>
        /// <returns></returns>
        public override Type BindToType(string assemblyName, string typeName)
        {
            if (ApplicationAssemblies.Contains(assemblyName) && TryGetCachedType(typeName, out Type type))
            {
                return type;
            }

            return GetTypeFromStack(typeName);
        }

        /// <summary>
        /// Gets Type using type cache
        /// </summary>
        /// <param name="typeName"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        private bool TryGetCachedType(string typeName, out Type type)
        {
            if (_cachedTypes.TryGetValue(typeName, out type))
            {
                return type != null;
            }

            type = GetType(typeName);

            if (type != null)
            {
                _cachedTypes.Add(typeName, type);

                return true;
            }

            _cachedTypes.Add(typeName, null);

            return false;
        }

        /// <summary>
        /// Gets type from core assemblies
        /// </summary>
        /// <param name="typeName"></param>
        /// <returns></returns>
        public static Type GetType(string typeName)
        {
            foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                Type type = assembly.GetType(typeName);
                if (type != null)
                {
                    return type;
                }
            }

            return Type.GetType($"{typeName}, {Assembly.GetExecutingAssembly().FullName}");
        }

        /// <summary>
        /// Search assemblies for type loaded by plugins or extensions
        /// </summary>
        /// <param name="typeName"></param>
        /// <returns></returns>
        public static Type GetTypeFromStack(string typeName)
        {
            Type type;
            foreach (Assembly assembly in AssemblyRegister.assemblyStack)
            {
                type = assembly.GetType(typeName);
                if (type != null)
                {
                    return type;
                }
            }

            return null;
        }
    }
}
