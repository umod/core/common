﻿using System.Collections.Generic;
using System.Reflection;

namespace uMod.Common.Serialization
{
    public delegate void AssemblyLoaded(Assembly assembly);

    public delegate void RawAssemblyLoaded(byte[] rawAssembly);

    public delegate void AssemblyFileLoaded(string path);

    public static class AssemblyRegister
    {
        internal static Stack<Assembly> assemblyStack = new Stack<Assembly>();

        public static event AssemblyLoaded OnAssemblyLoaded;

        public static event RawAssemblyLoaded OnRawAssemblyLoaded;

        public static event AssemblyFileLoaded OnAssemblyFileLoaded;

        private static void Loaded(Assembly assembly)
        {
            assemblyStack.Push(assembly);
            OnAssemblyLoaded?.Invoke(assembly);
        }

        public static void Loaded(Assembly assembly, byte[] rawAssembly)
        {
            Loaded(assembly);
            OnRawAssemblyLoaded?.Invoke(rawAssembly);
        }

        public static void Loaded(Assembly assembly, string assemblyPath)
        {
            Loaded(assembly);
            OnAssemblyFileLoaded?.Invoke(assemblyPath);
        }
    }
}
