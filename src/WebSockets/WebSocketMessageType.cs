﻿using System;

namespace uMod.Common.WebSockets
{
    [Serializable]
    public enum WebSocketMessageType
    {
        SocketPacket = 0,
        SocketStart = 1,
        SocketClose = 2,
        SocketStatus = 3,
        ServerStart = 4,
        ServerClose = 5,
        ServerStatus = 6,
        Error = 7,
        Exit = 8,
        Ready = 9,
        AggregatePacket = 10
    }
}
