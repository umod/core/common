﻿using System;

namespace uMod.Common
{
    [Serializable]
    public enum WebSocketConnectionState
    {
        Closed,
        Open
    }

    /// <summary>
    /// Connection interface
    /// </summary>
    public interface IWebSocketConnection
    {
        string Id {get;}
        string Address {get;}
        int Port {get;}
        WebSocketConnectionState State {get;}

        void Close(int code = 1000);

        void Send(string data);
        void Send(object data);
        void Send(byte[] data);
        void Send<T>(T data);
    }
}
