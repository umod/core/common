﻿using System;
using System.Runtime.Serialization;

namespace uMod.Common.WebSockets
{
    [Serializable]
    public sealed class WebSocketMessage
    {
        public object Data { get; set; }

        public object ExtraData { get; set; }

        public object ExtraData2 { get; set; }

        public int Id { get; set; }

        public string ClientId {get;set;}

        public WebSocketMessageType Type { get; set; }

        public override string ToString()
        {
            return $"{Type}: {Id}";
        }
    }
}
