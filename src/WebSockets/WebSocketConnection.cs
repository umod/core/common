﻿using uMod.Common;
using System;
using System.Runtime.Serialization;

namespace uMod.Common.WebSockets
{
    [Serializable]
    public class WebSocketConnection : IWebSocketConnection
    {
        public virtual string Id {get;}

        public virtual string Address {get;}

        public virtual int Port {
            get;
        }

        public virtual string Path
        {
            get;
        }

        public virtual WebSocketConnectionState State {
            get;
        }

        public virtual string ConnectionName
        {
            get;
        }

        public virtual void Close(int code = 1000) {}

        public virtual void Send(string data) {}

        public virtual void Send(byte[] data) {}

        public virtual void Send(object data) {}

        public virtual void Send<T>(T data) {}
    }
}
