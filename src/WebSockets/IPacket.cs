﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace uMod.Common.WebSockets
{
    public interface IPacket
    {
        string Identifier {get;}
        string Type {get;}
        string Message {get;}
        string Name {get;}
    }

    [Serializable]
    public class Packet : IPacket
    {
        public string Identifier {get; set;}

        public string Type {get; set;}

        public string Message {get; set;}

        public virtual string Name {get;set;}
    }
}
