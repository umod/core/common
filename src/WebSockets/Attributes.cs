﻿using System;

namespace uMod.Common.WebSockets
{
    /// <summary>
    /// Specify class as packet scope
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class PacketScopeAttribute : Attribute
    {
        public string Type;

        public PacketScopeAttribute(string type = null)
        {
            Type = type;
        }
    }
}
