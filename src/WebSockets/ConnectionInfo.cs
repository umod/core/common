﻿using System;

namespace uMod.Common.WebSockets
{
    [Serializable]
    public enum ConnectionState
    {
        Closed,
        Open
    }

    [Serializable]
    public class ConnectionInfo
    {
        public string Name;
        public string Protocol;
        public string Location;
        public int Port;
        public bool SupportDualStack = true;
        public string Path;
        public float IdleTimeoutMinutes;
        public bool Routing = true;

        public override string ToString()
        {
            return $"Connection: {Name} ({Protocol}://{Location}:{Port})";
        }
    }
}
