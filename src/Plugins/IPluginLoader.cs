﻿using System;
using System.Collections.Generic;
using System.IO;

namespace uMod.Common
{
    public enum PluginFailureReason
    {
        ConfigError,
        LocalizationError,
        Unknown
    }

    public interface IPluginLoader
    {
        Type[] CorePlugins { get; }
        string FileExtension { get; }
        bool HasLoadedCorePlugins { get; }

        ICollection<string> LoadingPlugins { get; }

        IDictionary<string, IPlugin> LoadedPlugins { get; }

        IDictionary<string, string> PluginErrors { get; }

        IDictionary<string, PluginFailureReason> PluginFailureReasons { get; }

        void LoadCorePlugins();

        IEnumerable<FileInfo> ScanDirectory(string directory);

        IEnumerable<string> Scan();

        IPlugin Load(string name);

        IPlugin Load(string directory, string name);

        void Load(IPlugin plugin, bool async = false);

        void Reload(string name);

        void Reload(string directory, string name);

        void Unloading(IPlugin plugin);

        IPromise<bool> PluginLoaded(IPlugin plugin);

        bool PluginUnloaded(IPlugin plugin);
    }

    public interface IBatchPluginLoader : IPluginLoader
    {
        void Reset();

        void RegisterPlugin(params Type[] pluginTypes);

        void UnregisterPlugin(params Type[] pluginTypes);
    }
}
