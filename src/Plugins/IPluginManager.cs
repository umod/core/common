﻿using System;
using System.Collections.Generic;

namespace uMod.Common
{
    public interface IPluginManager : IContextManager<IPlugin>
    {
        ILogger Logger { get; }

        bool Exists(string name);

        IPlugin GetPlugin(string name);

        T GetPlugin<T>(string name = null) where T : IPlugin;

        IEnumerable<IPlugin> GetPlugins();

        IEnumerable<T> GetPlugins<T>() where T : IPlugin;

        [Obsolete("Use GetPlugins instead")]
        IEnumerable<IPlugin> GetAll();

        [Obsolete]
        T Find<T>(string name = null) where T : IPlugin;

        [Obsolete]
        IPlugin Find(string name);

        [Obsolete]
        bool RemovePlugin(IPlugin plugin);

        object CallHook(string name, params object[] args);

        object CallHook(string name, object[] args, IEventArgs e = null);

        object CallHook(IHookName hookName, object[] args, IEventArgs e = null);

        object CallDeprecatedHook(string oldHook, string newHook, DateTime expireDate, params object[] args);

        object CallDeprecatedHook(string oldHook, string newHook, DateTime expireDate, object[] args, IEventArgs e = null);

        bool IsSubscribedToHook(string hook);

        bool IsSubscribedToHook(string hook, IPlugin plugin);

        bool IsSubscribedToHook(string hook, IPlugin plugin, Type[] parameterFilter);

        void SubscribeToHook(string hook, IPlugin plugin);

        void UnsubscribeToHook(string hook, IPlugin plugin);
    }
}
