﻿namespace uMod.Common
{
    public interface IChangeWatcher
    {
        string Directory { get; }

        IEvent<string> OnAdded { get; }

        IEvent<string> OnChanged { get; }

        IEvent<string> OnRemoved { get; }

        void StartWatcher();

        void StopWatcher();

        void AddMapping(string name);

        void AddMappings(params string[] names);

        void RemoveMapping(string name);

        void RemoveMappings(params string[] names);
    }
}
