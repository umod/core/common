﻿namespace uMod.Common
{
    public interface IPluginCommandConfiguration
    {
        char[] ChatCommandPrefixes { get; }
        bool UnknownCommands { get; }
        bool DeniedCommands { get; }
    }

    public interface IPluginSecurityConfiguration
    {
        bool Sandbox { get; }
    }

    public interface IPluginWatcherConfiguration
    {
        bool ConfigWatchers { get; }
        bool PluginWatchers { get; }
        string[] PluginDirectories { get; }
        string[] ExcludeDirectories { get; }
    }

    public interface IPluginConfiguration
    {
        IPluginCommandConfiguration Commands { get; }
        IPluginSecurityConfiguration Security { get; }
        IPluginWatcherConfiguration Watchers { get; }
    }
}
