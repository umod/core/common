﻿using System;
using uMod.Common.Plugins;

namespace uMod.Common
{
    public interface IPluginDiagnostics
    {
        double TotalHookTime { get; }

        int TrackStart();

        void TrackEnd(int startTime);
    }

    /// <summary>
    /// Plugin interface
    /// </summary>
    public interface IPlugin : IResource, IManagedContext<IPlugin, IPluginManager>
    {
        string Description { get; }

        bool HookedOnFrame { get; }
        bool IsCorePlugin { get; }
        IPluginLoader Loader { get; }
        bool IsLoaded { get; }
        bool IsResolved { get; }
        IPluginDiagnostics Diagnostics { get; }
        IPluginCommandHandler Commands { get; }

        IEvent<IPlugin, string> OnError { get; }
        IEvent<IPlugin, string> OnWarning { get; }
        IEvent<IPlugin, string, Exception> OnException { get; }
        IEvent<IPlugin> OnResolved { get; }

        void Load(IPluginLoader loader);

        void Unload(IPluginLoader loader);

        object CallHook(string hook, params object[] args);

        object CallHook<TArg1>(string hook, TArg1 obj1);

        object CallHook<TArg1, TArg2>(string hook, TArg1 obj1, TArg2 obj2);

        object CallHook<TArg1, TArg2, TArg3>(string hook, TArg1 obj1, TArg2 obj2, TArg3 obj3);

        object CallHook<TArg1, TArg2, TArg3, TArg4>(string hook, TArg1 obj1, TArg2 obj2, TArg3 obj3, TArg4 obj4);

        object CallHook<TArg1, TArg2, TArg3, TArg4, TArg5>(string hook, TArg1 obj1, TArg2 obj2, TArg3 obj3, TArg4 obj4, TArg5 obj5);

        object CallHook<TArg1, TArg2, TArg3, TArg4, TArg5, TArg6>(string hook, TArg1 obj1, TArg2 obj2, TArg3 obj3, TArg4 obj4, TArg5 obj5, TArg6 obj6);

        object CallHook(string hook, object[] args = null, IEventArgs e = null);

        object CallHook(IHookName hook, object[] args = null, IEventArgs e = null);

        object Call(string hook, params object[] args);

        T Call<T>(string hook, params object[] args);

        bool Resolve(IPluginLoader loader, string name = null, string fileName = null);
    }
}
