﻿namespace uMod.Common
{
    /// <summary>
    /// Nested context interface
    /// </summary>
    public interface INestedContext : IContext
    {
        IContext Context { get; }
    }

    /// <summary>
    /// Context manager interface
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IContextManager<T> where T : IContext
    {
        IEvent<T> OnContextAdded { get; }

        IEvent<T> OnContextRemoved { get; }

        bool AddContext(T context);

        bool RemoveContext(T context);

        void RemoveAll();
    }

    /// <summary>
    /// Context interface
    /// </summary>
    public interface IContext : INameable
    {
    }
}
