﻿namespace uMod.Common
{
    /// <summary>
    /// Cache interface
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    public interface ICache<TKey, TValue>
    {
        TValue this[TKey key] { get; }

        void Add(TKey key, TValue value, int expires = 0);

        bool ContainsKey(TKey key);

        bool TryGetValue(TKey key, out TValue value);

        void Forget(TKey key);

        void Clear();
    }
}
